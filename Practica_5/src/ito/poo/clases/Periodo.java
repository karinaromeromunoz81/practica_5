package ito.poo.clases;

public class Periodo {
	
		private String tiempodeCosecha;
		private float cantCosechaportiempo;

		public Periodo() {
			super();
		}
		
		public Periodo(String tiempodeCosecha, float cantCosechaportiempo) {
			super();
			this.tiempodeCosecha = tiempodeCosecha;
			this.cantCosechaportiempo = cantCosechaportiempo;
		}
		
		public String getTiempodeCosecha() {
			return tiempodeCosecha;
		}

		public void setTiempodeCosecha(String tiempodeCosecha) {
			this.tiempodeCosecha = tiempodeCosecha;
		}

		public float getCantCosechaportiempo() {
			return cantCosechaportiempo;
		}

		public void setCantCosechaportiempo(float cantCosechaportiempo) {
			this.cantCosechaportiempo = cantCosechaportiempo;
		}
		
		@Override
		public String toString() {
			return "(Tiempo de cosecha: " + tiempodeCosecha + ", Cantidad de cosecha: " + cantCosechaportiempo + ")";
		}

}