package ito.poo.clases;
import java.util.ArrayList;
public class Fruta {
	
	

		private String nombre;
		private float extension;
		private float costoPromedio;
		private float precioVentaPromedio;
		private ArrayList<Periodo> periodos = new ArrayList<Periodo>();
		//////////////////////////////////////////////////////////////////////////////////////////////////////////
		public Fruta() {
			super();
		}

		public Fruta(String nombre, float extension, float costoPromedio, float precioVentaPromedio) {
			super();
			this.nombre = nombre;
			this.extension = extension;
			this.costoPromedio = costoPromedio;
			this.precioVentaPromedio = precioVentaPromedio;
		}
		//////////////////////////////////////////////////////////////////////////////////////////////////////////
		public String getNombre() {
			return nombre;
		}

		public void setNombre(String nombre) {
			this.nombre = nombre;
		}

		public float getExtension() {
			return extension;
		}

		public void setExtension(float extension) {
			this.extension = extension;
		}

		public float getCostoPromedio() {
			return costoPromedio;
		}

		public void setCostoPromedio(float costoPromedio) {
			this.costoPromedio = costoPromedio;
		}

		public float getPrecioVentaPromedio() {
			return precioVentaPromedio;
		}

		public void setPrecioVentaPromedio(float precioVentaPromedio) {
			this.precioVentaPromedio = precioVentaPromedio;
		}

		public ArrayList<Periodo> getPeriodos() {
			return this.periodos;
		}
		/////////////////////////////////////////////////////////////////////////////////////////////////////////
		public void agregarPeriodo(String tiempoCosecha, float cantCosechaxtiempo) {
			Periodo p = new Periodo (tiempoCosecha, cantCosechaxtiempo);
			this.periodos.add(p);
		}
		
		public Periodo devolverelPeriodo(int i) {
			Periodo p;
			if (i > this.periodos.size() || i < 0)
				p = new Periodo ("null", 0);
			else 
				p = this.periodos.get(i);
			return p;
		}

		public boolean eliminarelPeriodo(int i) {
			boolean e;
			if (i > this.periodos.size() || i < 0)
				e = false;
			else {
				this.periodos.remove(i);
				e = true;
			}
			return e;
		}
		
		public float costoPeriodo(Periodo p) {
			float i = 0;
			if (i > this.periodos.size() || i < 0)
				i = 0;
			else 
				i = p.getCantCosechaportiempo() * this.costoPromedio;
			return i;
		}

		public float gananciaEstimada(Periodo p) {
			float i = 0;
			if (i > this.periodos.size() || i < 0)
				i = 0;
			else 
				i = (p.getCantCosechaportiempo() * this.precioVentaPromedio) - costoPeriodo(p);;
			return i;
		}
		
		@Override
		public String toString() {
			return "Fruta: " + nombre + "\nExtension: " + extension + "\nCosto Promedio: " + costoPromedio
					+ "\nPrecio Venta Promedio: " + precioVentaPromedio + "\nPeriodos: " + periodos;
		}

}