package ito.poo.clases;

import java.time.LocalDate;

public class CuentaBancaria {
	

		private long numCuenta = 0L;
		private String nomCliente = "";
		private float saldo = 0F;
		private LocalDate fechadeApertura = null;
		private LocalDate fechadeActualizacion = null;
		///////////////////////////////////////////////////////////////////////////////////////////////////////////
		public CuentaBancaria() {
			super();
		}
		public CuentaBancaria(long numCuenta, String nomCliente, float saldo, LocalDate fechadeApertura) {
			super();
			this.numCuenta = numCuenta;
			this.nomCliente = nomCliente;
			this.saldo = saldo;
			this.fechadeApertura = fechadeApertura;
		}
		///////////////////////////////////////////////////////////////////////////////////////////////////////////
		public boolean Deposito(float Cantidad,LocalDate newfechadeActualizacion) {
			boolean Deposito = false;
			if(this.fechadeApertura==null)
				System.out.println("Esta cuenta no esta activada");
			else {
				Deposito = true;
				this.setSaldo(this.getSaldo()+ Cantidad);
				this.setFechadeActualizacion(newfechadeActualizacion);
			}
			return Deposito;
		}
        
		public boolean Retiro (float Cantidad,LocalDate newFechadeActualizacion) {
			boolean Retiro = false;
			if (Cantidad <= this.getSaldo()) {
				 Retiro=true;
				 this.setSaldo(this.getSaldo()-Cantidad);
				 this.setFechadeActualizacion(newFechadeActualizacion);
				}
			else
				System.out.println("La cantidad que desea a retirar esta limitada ");
			return Retiro;
		}
		///////////////////////////////////////////////////////////////////////////////////////////////////////////
		public long getNumCuenta() {
			return numCuenta;
		}

		public void setNumCuenta(long numCuenta) {
			this.numCuenta = numCuenta;
		}

		public String getNomCliente() {
			return nomCliente;
		}

		public float getSaldo() {
			return saldo;
		}

		public void setSaldo(float saldo) {
			this.saldo = saldo;
		}

		public LocalDate getFechadeApertura() {
			return fechadeApertura;
		}

		public void setFechadeApertura(LocalDate fechadeApertura) {
			this.fechadeApertura = fechadeApertura;
		}

		public LocalDate getFechadeActualizacion() {
			return fechadeActualizacion;
		}

		public void setFechadeActualizacion(LocalDate fechadeActualizacion) {
			this.fechadeActualizacion = fechadeActualizacion;
		}
		/***/
		@Override
		public String toString() {
			return "CuentaBancaria [numCuenta=" + numCuenta + ", nomCliente=" + nomCliente + ", saldo=" + saldo
					+ ", fechadeApertura=" + fechadeApertura + ", fechadeActualizacion=" + fechadeActualizacion + "]";
		}
	

}