package ito.poo.clases;

import java.time.LocalDate;
	
import java.util.ArrayList;

public class Prenda {
	


		
		private int Modelo;
		private String Tela;
		private float CostodeProduccion;
		private String Genero;
		private String Temporada;
		private ArrayList<Lote> lotes = new ArrayList<Lote>();
		//////////////////////////////////////////////////////////////////////////////////////////////////////////////
		public Prenda() {
			super();
		}
		
		public Prenda(int Modelo, String Tela, float CostodeProduccion, String Genero, String Temporada) {
			super();
			this.Modelo = Modelo;
			this.Tela = Tela;
			this.CostodeProduccion = CostodeProduccion;
			this.Genero = Genero;
			this.Temporada = Temporada;
		}
		/********/
		public void setModelo(int Modelo) {
			this.Modelo = Modelo ;
		}

		public String getTela() {
			return Tela;
		}

		public void setTela(String Tela) {
			this.Tela = Tela;
		}

		public float getCostodeProduccion() {
			return CostodeProduccion;
		}

		public void setCostodeProduccion(float CostodeProduccion) {
			this.CostodeProduccion = CostodeProduccion;
		}

		public String getGenero() {
			return Genero;
		}

		public void setGenero(String Genero) {
			this.Genero = Genero;
		}
		public String getTemporada() {
			return Temporada;
		}

		public void setTemporada(String Temporada) {
			this.Temporada = Temporada;
		}

		//////////////////////////////////////////////////////////////////////////////////////////////////////////
		public void addLote(int a, int b, LocalDate c) {
			Lote e = new Lotes(a, b, c);
			this.lotes.add(e);
		}
		
		public Lote getLote(int i){
			Lote e = null;
			if (i > 1 || i < this.lotes.size())
				e = this.lotes.get(i - 1);
			return e;
		}
		/********/
		public float costoxLote(float costoxUnidad) {
			float i = 0;
			return i;
		}
		/********/
		@Override
		public String toString() {
			return "Prenda [Modelo=" + Modelo + ", Tela=" + Tela + ", CostodeProduccion=" + CostodeProduccion + ", Genero="
					+ Genero + ", Temporada=" + Temporada + ", lotes=" + lotes + "]";
		}
}